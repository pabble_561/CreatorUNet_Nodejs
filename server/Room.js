class Room{
    constructor(game,id){
        this.game = game;
        this.id = id;
        this.users = [];
        this.gameData = {};

        this.scene_data = {};
        this.scene_user = {};
        this.frameData = {};
        this.frameTime = 0
    }

    frameUpdate(dt){
        this.frameTime ++;
        this.broadcast('frame',{t:this.frameTime,v:this.frameData});
        this.frameData = {};
    }

    sceneInfo(){
        return {users:this.users,scene_user:this.scene_user,scene_data:this.scene_data}
    }

    onUserEnter(user){
        var old = this.users.findIndex(v=>{return v.id == user.id});
        if(old == -1){
            user.pid = this.users.length;
            this.gameBroadcast('userEnter',{user:user});
            this.users.push(user);
            this.scene_user[user.pid] = {}
            this.gameSend(user,'join',Object.assign({pid:user.pid},this.sceneInfo()))
        } else {
            user.pid = this.users[old].pid
            this.users[old].line = 0
            this.gameBroadcast('userEnter',{user:user});
            this.gameSend(user,'join',Object.assign({pid:this.users[old].pid},this.sceneInfo()))
        }
    }
    onUserExit(user,reason='auto'){
        var idx = this.users.findIndex(item=>{return user.id == item.id})
        if(idx >= 0){
            user.pid = this.users[idx].pid
            this.users[idx].line = 1
            // delete this.scene_user[this.users[idx].pid]
            // this.users.splice(idx,1);

            console.log('用户掉线，剩余玩家:',this.users.length);
            this.gameBroadcast('userExit',{pid:user.pid,reason});
        }
    }

    assignment(a,b){
        for(var k in b){
            if(typeof(b[k]) == 'string' || typeof(b[k]) == 'number'){
                a[k] = b[k]
            } else if(typeof(b[k]) == 'object'){
                a[k] = (b[k].constructor === Array  ? [] : {});
                a[k] = JSON.parse(JSON.stringify(b[k]))
            }
        }
    }
    message(user,data){
        var u1 = this.users.find(v=>{return v.id == user.id});
        var pid = u1.pid
        user.pid = pid
        console.log('message:',data.ge,this.users.length);
        if(data.ge == 'ready'){
            for(let i=0; i<this.users.length; i++){
                if(this.users[i].id == user.id){
                    this.gameSend(user,'ready',{});
                }
            }
        } else if(data.ge == 'suser'){
            this.assignment(this.scene_user[pid],data.v);
        } else if(data.ge == 'sdata'){
            this.assignment(this.scene_data,data.v);
        } else if(data.ge == 'frame'){
            if(!this.frameData[pid]){
                this.frameData[user.id] = {}
            }
            this.frameData[pid] = data.v;
        } else {
            this.gameBroadcast(data.ge,data.v);
        }
    }
    gameSend(user,e,v){
        this.game.send(user.id,{e:'game',ge:e,v:v});
    }
    sendToUser(user,e,v){
        this.game.send(user.id,{e:e,v:v});
    }
    gameBroadcast(e,v){
        for(let i=0; i<this.users.length; i++){
            this.gameSend(this.users[i],e,v)
        }
    }
    broadcast(k,v){
        for(let i=0; i<this.users.length; i++){
            this.sendToUser(this.users[i],k,v)
        }
    }
}



module.exports = Room;