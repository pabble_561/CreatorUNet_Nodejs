// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UNetIdentity from "../UNet/UNetIdentity";
import { UNetMgr } from "../UNet/UNetMgr";


const {ccclass, property} = cc._decorator;

var LOCAL_USER = ['0','1','2','3']

@ccclass
export default class RoomControl extends cc.Component {
    @property(cc.EditBox)
    roomId : cc.EditBox = null;
    @property(cc.EditBox)
    roleId : cc.EditBox = null;

    @property(cc.Prefab)
    rolePrefab : cc.Prefab = null;

    onLoad(){
        UNetMgr.model = UNetMgr.SyncModel.SelfNoDelay;

        UNetMgr.event.on('join',(v)=>{
            console.log('加入房间:',v);
            this.node.active = false;
            cc.find('Canvas/Game').active = true;
        },null)

        UNetMgr.event.on('loginSuccess',function(v){
            console.log('登陆完成:',v);
            UNetMgr.JoinRoom('1','0')
        },null)
    }
    start(){
    }

    onClickEnter(e,d){
        var loginData = {id:LOCAL_USER[d]}

        UNetMgr.StartConnect({
            isSSL : false, // wss 时参数为true，并且提供证书的cert路径
            cert:'', //相对于resource的路径
            host:'localhost', 
            port : '8001',
            autoConnect:true //非人为断开时自动重连
        },loginData)

        //角色预制件托管 默认添加在Canvas
        UNetMgr.room.rolePrefab = this.rolePrefab;
        // 如果不想由框架拖管，或者有自己的层级管理，参考下列代码
        // UNetMgr.room.instantiateRole = function(){
        //     var role = cc.instantiate(this.rolePrefab);
        //     cc.find('Canvas').addChild(role);
        //     var idt = role.getComponent(UNetIdentity);
        //     return idt
        // }

    }
}
