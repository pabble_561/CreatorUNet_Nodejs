

const {ccclass, property} = cc._decorator;

@ccclass
export default abstract class FrameStep extends cc.Component {

    /** 创建帧数 */
    createFrame : number = 0;

    /** 创建时的数据 */
    createData : any = null;

    /** 上次更新的帧值 */
    lastUpdateFrame : number = 0;

    lockStepTween : cc.Tween = null

    _uuid : string = '';

    initFrame(id:string,frame:number,data:any){
        this._uuid = id;
        this.createFrame = frame;
        this.createData = data;
        this.lastUpdateFrame = frame
    }

    /** 帧同步主要函数 */
    abstract OnSyncFrame(frame:number);
}
